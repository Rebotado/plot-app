import PySimpleGUI as sg
import ast
import re
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib

class PlotApplication:

    def __init__(self, title='Title', coordinates_label='Enter Coordinates', close_window_label='Close Window', plot_label='Plot',
    error_message='Enter a valid format of coordinates', error_title='Error'):
        matplotlib.use('TkAgg')
        self.title = title
        self.coordinates_label = coordinates_label
        self.close_window_label = close_window_label
        self.plot_label = plot_label
        self.error_message = error_message
        self.error_title = error_title
        
    def _string_to_coordinates(self, coordinates):
        regex = r'\[\d,\d\]'
        prefix = r'[\[\{\(]'
        suffix = r'[\]\}\)]'
        coordinates = re.sub(prefix, '[', coordinates)
        coordinates = re.sub(suffix, ']', coordinates)
        array = re.findall(regex, coordinates)
        return [ast.literal_eval(x) for x in array]

    def _plot_coordinates(self, coordinates):
        coordinates = [[x,y] for x,y in coordinates]
        coordinates.append(coordinates[0])
        x,y = zip(*coordinates)
        self.plt.plot(x)


    def _draw_figure(self, canvas, figure):
        figure_canvas_agg = FigureCanvasTkAgg(figure, canvas)
        figure_canvas_agg.draw()
        figure_canvas_agg.get_tk_widget().pack(side='top', fill='both', expand=1)
        return figure_canvas_agg

    def _retrieve_layout(self):
        return [
            [sg.Text(self.coordinates_label), sg.InputText(key='txtbox_coordinates')],
            [sg.Canvas(key='-CANVAS-')],
            [sg.Button(self.plot_label), sg.Button(self.close_window_label)]
        ]

    def _draw_application(self):
        fig = matplotlib.figure.Figure(figsize=(5,4), dpi=100)
        self.plt = fig.add_subplot(111)
        self.window = sg.Window(self.title, self._retrieve_layout()).Finalize()
        self.fig_canvas_agg = self._draw_figure(self.window['-CANVAS-'].TKCanvas, fig)

    def run(self):
        self._draw_application()
        while True:
            event, values = self.window.read()
            if event in(None, self.close_window_label):
                break
            elif event in (None, self.plot_label):
                coordinates = values['txtbox_coordinates']
                try:
                    coordinates = self._string_to_coordinates(coordinates)
                    self._plot_coordinates(coordinates)
                    self.fig_canvas_agg.draw()
                except:
                    sg.Popup(self.error_title, self.error_message)
        self.window.close()

PlotApplication().run()