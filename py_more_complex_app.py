import PySimpleGUI as sg
import ast
import re
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib
matplotlib.use('TkAgg')


def plot_coordinates(plt, coordinates, line_color=None, fill_color=None, x_offset=0, y_offset=0):
    coordinates = [[x + x_offset,y + y_offset] for x,y in coordinates]
    coordinates.append(coordinates[0])
    x,y = zip(*coordinates)
    plt.plot(x, y, color=line_color)
    if fill_color is not None:
        plt.fill_between(x,y, color=fill_color)

def string_to_array_of_int(string):
    regex = r'\[\d,\d\]'
    prefix = r'[\[\{\(]'
    suffix = r'[\]\}\)]'
    string = re.sub(prefix, '[', string)
    string = re.sub(suffix, ']', string)
    array = re.findall(regex, string)
    return [ast.literal_eval(x) for x in array]

def draw_figure(canvas, figure):
    figure_canvas_agg = FigureCanvasTkAgg(figure, canvas)
    figure_canvas_agg.draw()
    figure_canvas_agg.get_tk_widget().pack(side='top', fill='both', expand=1)
    return figure_canvas_agg

# All the stuff inside your window.
layout = [ 
            [sg.Text('Enter coordinates'), sg.InputText(key='txtbox_coordinates'),],
            [sg.Canvas(key='-CANVAS-')],
            [sg.Button('Plot'), sg.Button('Close Window')]
            ]  # identify the multiline via key option

#Create plt
fig = matplotlib.figure.Figure(figsize=(5,4), dpi=100)
plt = fig.add_subplot(111)
# Create the Window
window = sg.Window('Test', layout).Finalize()
fig_canvas_agg = draw_figure(window['-CANVAS-'].TKCanvas, fig) #adds plot to window

while True:
    event, values = window.read()
    if event in (None, 'Close Window'): # if user closes window or clicks cancel
        break
    elif event in (None, 'Plot'):
        coordinates = values['txtbox_coordinates'] #get the value form textbox
        try:
            coordinates = string_to_array_of_int(coordinates)
            plot_coordinates(plt, coordinates)
            fig_canvas_agg.draw()
        except:
            sg.Popup('Error', 'Enter a valid format of coordinates')

window.close()