import PySimpleGUI as sg

#Create basic layout with one text and one button
layout = [[sg.Text("Hello World! from PySimpleGUI")], [sg.Button("OK")]]

# Create the window
window = sg.Window("Demo GUI", layout)

# Create an event loop
while True:
    event, values = window.read()
    # End program if user closes window or
    # presses the OK button
    if event == "OK" or event == sg.WIN_CLOSED:
        break

window.close()